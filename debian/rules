#!/usr/bin/make -f

export DEB_BUILD_MAINT_OPTIONS = hardening=+all

# generate documentation unless nodoc requested
ifeq (,$(filter nodoc,$(DEB_BUILD_OPTIONS)))
DOCS = README.html README.txt
endif

UTF16_FILES = libatalk/unicode/utf16_case.c libatalk/unicode/utf16_casetable.h

%:
	dh $@

%.html: %.md
	cmark-gfm $< > $@

%.txt: %.md
	cmark-gfm --to plaintext $< > $@

execute_after_dh_auto_build: $(DOCS)

override_dh_installdocs:
	dh_installdocs -- $(DOCS)

# restore stale unicode tables moved aside during build
execute_after_dh_auto_clean:
	for file in $(UTF16_FILES); do \
		[ ! -f $$file.save ] || mv --force $$file.save $$file; \
	done

# move aside stale unicode tables and generate up-to-date tables
execute_before_dh_auto_configure:
	for file in $(UTF16_FILES); do \
		[ ! -f $$file ] || mv $$file $$file.save; \
	done
	contrib/shell_utils/make-casetable.pl \
		/usr/share/unicode/UnicodeData.txt \
		libatalk/unicode/utf16_casetable.h \
		libatalk/unicode/utf16_case.c

override_dh_auto_configure:
	dh_auto_configure -- \
		--without-tdb \
		--without-talloc \
		--localstatedir=/var/lib \
		--with-pkgconfdir=/etc/netatalk \
		--with-dbus-daemon=/usr/bin/dbus-daemon \
		--with-tracker-pkgconfig-version=3.0 \
		--with-libgcrypt-dir \
		--with-libtirpc \
		--with-ssl-dir \
		--enable-pgp-uam \
		--enable-krbV-uam \
		--with-cracklib=/var/cache/cracklib/cracklib_dict \
		--with-init-style=debian-systemd

# Install sysV initscript with debhelper to add pre- and postinst routines
execute_before_dh_install:
	cp distrib/initscripts/rc.debian debian/netatalk.init

execute_after_dh_install:
	rm debian/netatalk/usr/bin/netatalk-config
	rm debian/netatalk/usr/lib/$(DEB_HOST_MULTIARCH)/libatalk.a
	rm debian/netatalk/usr/lib/$(DEB_HOST_MULTIARCH)/libatalk.la
	rm debian/netatalk/usr/share/man/man1/afppasswd.1
	rm debian/netatalk/usr/share/man/man1/netatalk-config.1
	rm debian/netatalk/usr/share/man/man1/uniconv.1
	rm -r debian/netatalk/usr/include
	rm -r debian/netatalk/usr/share/aclocal
	rm debian/netatalk/usr/lib/$(DEB_HOST_MULTIARCH)/netatalk/*.la

override_dh_strip:
	dh_strip --dbgsym-migration='netatalk-dbg (<< 3.1.12~ds-7~)'
